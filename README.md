# military_funding

Data downloaded from https://www.usaspending.gov/#/download_center/award_data_archive
Filtered by
agency = DOD
Award Type = Financial Assistance

Years 2015, 2016, 2017.

Simple reading in of data, filtering by educational institutions, aggregating funding sums, etc.

Government provided visualization of the data can be found here: https://datalab.usaspending.gov/colleges-and-universities/

Further coarse data source: https://www.aaas.org/sites/default/files/UniAgency1.xlsx?IZcy4cUGWz_2Y.BMOsc2jnkGdLB.uIbm